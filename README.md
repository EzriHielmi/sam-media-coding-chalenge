# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* SAM Media Coding Chalenge
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Description ###

* Basically, this is just a simple PHP codes,
 	which is actually 100 % working for a simple search purpose.
	However, due the large datasets, no database were created for 
	this particular test. 
-This codes are divided into two parts:
	1. search_place.php
	2.show_api.php
	
	
1. search_place.php
	-The codes used in this file are executable. After inserting the keywords,
	 the page will be directed to show_api.php
	 
2. show_api.php
	-upon the keywords used in the search_place.php, this page will return 
	relevant values from both State and Location fields. Let say you insert
	the keyword "Kuala", this page will return values State and Location Values 
	that have "kuala" word. This is done to simplify users to search as what most
	search engine do. Only relevant keywords will be returned.